include("GeneratorUtils/StdEvgenSetup.py")
theApp.EvtMax = 10000

import ParticleGun as PG
pg = PG.ParticleGun()
pg.randomSeed = 123456
pg.samplers[0].pid = (-13, 13) # cycle mu+-
#pg.samplers[0].mom = PG.EEtaMPhiSampler(energy=50000, eta=[2.49999,2.5], phi=[0,0.00001])
pg.samplers[0].mom = PG.PtEtaMPhiSampler(pt=200000, eta=[1.2,2.7], phi=[3.05, 3.25])
topSeq += pg

include("GeneratorUtils/postJO.CopyWeights.py")
include("GeneratorUtils/postJO.PoolOutput.py")
include("GeneratorUtils/postJO.DumpMC.py")


